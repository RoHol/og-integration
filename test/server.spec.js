const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const port = 3000;

describe("Color Code Converter API", () => {

    before("Start server before run tests", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening: localhost:${port}`);
            done();
        });
    });

    describe("RGB to Hex conversion", () => {
        const url = `http://localhost:${port}/rgb-to-hex?red=ff0000&green=00ff00&blue=0000ff`;

        it("Returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns color in hex", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("[0,255,0]");
                done();
            });
        });
    });

    after("Stop server after tests", (done) => {
        server.close();
        done();
    })
});