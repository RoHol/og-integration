const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("Hex to RGB conversion", () => {
    it("converts the basic colors", () => {
      const redRgb = converter.hexToRgb("ff0000");
      const greenRgb = converter.hexToRgb("00ff00");
      const blueRgb  = converter.hexToRgb("0000ff");

      expect(redRgb).to.deep.equal([255, 0, 0]);
      expect(greenRgb).to.deep.equal([0, 255, 0]);
      expect(blueRgb).to.deep.equal([0, 0, 255]);
    });
  });
});